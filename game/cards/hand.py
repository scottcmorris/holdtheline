from card import Card
from ConfigParser import ConfigParser
from random import Random


class Hand(object):
    def __init__(self, max_size):
        self.__cards = []
        self.max_size = max_size

    @property
    def cards(self):
        return self.__cards

    def add(self, card):
        self.__cards.append(card)

    def remove(self, card):
        self.__cards.remove(card)

    def count(self):
        return len(self.cards)

    def display(self):
        for card in self.cards:
            card.display()
        print ''

    def swap(self, card1, card2):
        i = self.__cards.index(card1)
        j = self.__cards.index(card2)

        self.__cards[i], self.__cards[j] = self.__cards[j], self.__cards[i]

    def reorder(self, options):
        if 'number' in options:
            self.__cards = sorted(self.__cards, key=lambda card: card.number, reverse=True)
        if 'suit' in options:
            self.__cards = sorted(self.__cards, key=lambda card: card.suit)

    def straight_flushes(self):
        self.reorder(options=['suit', 'number'])

        result = []
        x = self.cards
        y = self.cards[1:]
        z = self.cards[2:]

        i = 0
        while i < len(z):
            card1 = x[i]
            card2 = y[i]
            card3 = z[i]
            if ((card1.suit == card2.suit == card3.suit) and
                (card1.number == card2.number + 1 == card3.number + 2)):
                temp_lane = Hand(max_size=3)
                temp_lane.add(card1)
                temp_lane.add(card2)
                temp_lane.add(card3)
                result.append(temp_lane)
                i += 2
            i += 1

        return result

    def trips(self):
        self.reorder(options=['number'])
        result = []
        x = self.cards
        y = self.cards[1:]
        z = self.cards[2:]

        i = 0
        while i < len(z):
            card1 = x[i]
            card2 = y[i]
            card3 = z[i]
            if (card1.number == card2.number == card3.number):
                temp_lane = Hand(max_size=3)
                temp_lane.add(card1)
                temp_lane.add(card2)
                temp_lane.add(card3)
                result.append(temp_lane)
                i += 2
            i += 1

        return result

    def flushes(self):
        self.reorder(options=['suit'])
        result = []
        x = self.cards
        y = self.cards[1:]
        z = self.cards[2:]

        i = 0
        while i < len(z):
            card1 = x[i]
            card2 = y[i]
            card3 = z[i]
            if (card1.suit == card2.suit == card3.suit):
                temp_lane = Hand(max_size=3)
                temp_lane.add(card1)
                temp_lane.add(card2)
                temp_lane.add(card3)
                result.append(temp_lane)
                i += 2
            i += 1

        return result

    def straights(self):
        result = []
        freq_dict = {}
        for card in self.cards:
            if card.number not in freq_dict:
                freq_dict[card.number] = []
            freq_dict[card.number].append(card)

        i = 10
        while i > 2:
            if ((i in freq_dict and len(freq_dict[i]) > 0) and
                    (i - 1 in freq_dict and len(freq_dict[i - 1]) > 0) and
                    (i - 2 in freq_dict and len(freq_dict[i - 2]) > 0)):
                card1 = freq_dict[i][0]
                card2 = freq_dict[i - 1][0]
                card3 = freq_dict[i - 2][0]

                temp_lane = Hand(max_size=3)
                temp_lane.add(card1)
                temp_lane.add(card2)
                temp_lane.add(card3)
                result.append(temp_lane)

                freq_dict[i].remove(card1)
                freq_dict[i - 1].remove(card2)
                freq_dict[i - 2].remove(card3)
            else:
                i -= 1

        return result


class Deck(Hand):
    def __init__(self, game_type='battleline'):
        parser = ConfigParser()
        parser.read('config/game.properties')
        suits = parser.get(game_type, 'suits').split(',')
        numbers = [int(number) for number in parser.get(game_type, 'numbers').split(',')]
        max_size = len(suits) * len(numbers)

        super(Deck, self).__init__(max_size)

        for suit in suits:
            for number in numbers:
                self.add(Card(suit, number))

        self.shuffle()

    def shuffle(self):
        randomness = Random()

        for i in range(self.count() - 1, -1, -1):
            j = randomness.randint(0, i)

            self.swap(self.cards[i], self.cards[j])


    def deal(self):
        return self.cards.pop(0)


    def deal_all(self, hands=[]):
        while True:
            for hand in hands:
                if self.count() > 0:
                    hand.draw(self.deal())
                else:
                    return hands


class PlayingHand(Hand):
    def __init__(self, max_size=5):
        super(self.__class__, self).__init__(max_size)

    def draw(self, card):
        if self.count() < self.max_size:
            self.add(card)
        else:
            raise OverflowError

    def play(self, card):
        if card in self.cards:
            self.remove(card)
        else:
            raise IndexError


class Lane(Hand):
    def __init__(self):
        super(self.__class__, self).__init__(3)

    def is_flush(self):
        if self.count() < 3:
            return False
        return all(x.suit == y.suit for x, y in zip(self.cards, self.cards[1:]))

    def is_straight(self):
        self.reorder(options=['number'])
        if self.count() < 3:
            return False
        return all(x.number == y.number + 1 for x, y in zip(self.cards, self.cards[1:]))

    def is_trips(self):
        if self.count() < 3:
            return False
        return all(x.number == y.number for x, y in zip(self.cards, self.cards[1:]))

    def total(self):
        result = 0
        for card in self.cards:
            result += card.number
        return result
