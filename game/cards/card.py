class Card(object):
    def __init__(self, suit, number):
        self.__suit = suit
        self.__number = number

    def __str__(self):
        return '%s %s' % (self.suit, self.number)

    @property
    def suit(self):
        return self.__suit

    @property
    def number(self):
        return self.__number

    def display(self):
        print str(self)
