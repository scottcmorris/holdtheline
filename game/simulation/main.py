from optparse import OptionParser
from game import Deck, PlayingHand

def evaluate_hand(hand):
  score = [0, 0, 0, 0, 0]
  (score[0], hand) = straight_flush(hand)
  (score[1], hand) = trips(hand)
  (score[2], hand) = flush(hand)
  (score[3], hand) = straight(hand)
  (score[4], hand) = highs(hand)

  return sum(score)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-g', '--game',
                      dest='game_type', default='battleline',
                      help='Select a game type. Options: battleline, schottentotten')

    (options, args) = parser.parse_args()



    # team_one = [PlayingHand(15), PlayingHand(15)]
    # team_two = [PlayingHand(15), PlayingHand(15)]
    # hands = [team_one[0], team_two[0], team_one[1], team_two[1]]

    # hands = [PlayingHand(30), PlayingHand(30)]

    # deck = Deck()
    # deck.display()

    # hands = deal_all(deck, hands)
    # for hand in hands[0].straights():
    #   for card in hand.cards:
    #     card.display()

    # hands[0].display()
    # hands[1].display()

    # hands[0].reorder(options=['color'])
    # hands[0].reorder(options=['color', 'number'])
    # hands[0].display()
    # team_one[0].display()
    # team_one[1].display()

    test_results = {
        'sf_results': {},
        'tr_results': {},
        'fl_results': {},
        'st_results': {}
    }

    test_size = 10000

    for test in range(test_size):
        if test % 100 == 0:
            print 'Simulating deck %s/%s' % (test, test_size)
        hands = [PlayingHand(30), PlayingHand(30)]
        deck = Deck(options.game_type)
        hands = deck.deal_all(hands)
        hands[0].reorder(options=['suit', 'number'])

        sf = hands[0].straight_flushes()
        for hand in sf:
            for card in hand.cards:
                hands[0].remove(card)

        key = len(sf)
        if key not in test_results['sf_results']:
            test_results['sf_results'][key] = 1
        else:
            test_results['sf_results'][key] += 1

        tr = hands[0].trips()
        for hand in tr:
            for card in hand.cards:
                hands[0].remove(card)

        key = len(tr)
        if key not in test_results['tr_results']:
            test_results['tr_results'][key] = 1
        else:
            test_results['tr_results'][key] += 1

        fl = hands[0].flushes()
        for hand in fl:
            for card in hand.cards:
                hands[0].remove(card)

        key = len(fl)
        if key not in test_results['fl_results']:
            test_results['fl_results'][key] = 1
        else:
            test_results['fl_results'][key] += 1

        st = hands[0].straights()
        for hand in st:
            for card in hand.cards:
                hands[0].remove(card)

        key = len(st)
        if key not in test_results['st_results']:
            test_results['st_results'][key] = 1
        else:
            test_results['st_results'][key] += 1

    print test_results


    # for _ in range(0, 5):
    #   for hand in hands:
    #     hand.draw(deck.deal())

    # deck.display()

    # for hand in hands:
    #   hand.display()