import unittest
from game.cards.card import Card

class TestCard(unittest.TestCase):
    def __init__(self):
        super(TestCard, self).__init__()
        self.card = Card('Red', 2)

    def test_init(self):
        self.assertEquals(self.card.suit, 'Red')
        self.assertEquals(self.card.number, 2)
        self.assertEquals(str(self.card), 'Red 2')

    def test_type(self):
        self.assertIsInstance(self.card.suit, str)
        self.assertIsInstance(self.card.number, (int, float))

if __name__ == '__main__':
    unittest.main()